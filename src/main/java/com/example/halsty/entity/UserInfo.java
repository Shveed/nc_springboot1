package com.example.halsty.entity;

public interface UserInfo {

    String getFullInfo(String name, String country);

}
