package com.example.halsty.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserInfoService implements UserInfo {
    @Value("${gameUser.nickname}")
    private String nickname;

    @Value("${gameUser.country}")
    private String country;

    @Autowired
    private Company company;

    public String getFullInfo(String name, String country){
        name = (name == null ? this.nickname : name);
        country = (country == null ? this.country : country);
        return  name + " of " + this.company.getCompany() +
                " is playing from " + country;
    }
}
