package com.example.halsty.controller;

import com.example.halsty.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GameController {

    @Autowired
    private UserInfo gameUser;

    @GetMapping("/game")
    public String getInfo(@RequestParam(name="name", required = false) String name,
                          @RequestParam(name="country", required = false) String country){
        return gameUser.getFullInfo(name, country);
    }
}
